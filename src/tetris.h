#ifndef _H_TETRIS_H_
#define _H_TETRIS_H_

// Based on tutorial
// http://javilop.com/gamedev/tetris-tutorial-in-c-platform-independent-focused-in-game-logic-for-beginners/

#include <cstdint>
#include <functional>


namespace tetris {

    // Piece types
    typedef uint8_t PieceType;
    const static PieceType kPieceSquare = 0;
    const static PieceType kPieceI = 1;
    const static PieceType kPieceL = 2;
    const static PieceType kPieceLMirrored = 3;
    const static PieceType kPieceN = 4;
    const static PieceType kPieceNMirrored = 5;
    const static PieceType kPieceT = 6;
    const static PieceType kPieceUnknown = 7;

    // Block type
    typedef uint8_t BlockType;
    const static BlockType kNoBlock = 0;
    const static BlockType kNormalBlock = 1;
    const static BlockType kPivotBlock = 2;

    // Pieces definition
    const static uint8_t kKindCount = 7;
    const static uint8_t kRotationCount = 4;
    const static uint8_t kHorizontalBlockCount = 5;
    const static uint8_t kVerticalBlockCount = 5;
    const static BlockType kPieces[kKindCount][kRotationCount][kHorizontalBlockCount][kVerticalBlockCount] =
    {
        // Square
        {
            {
                {0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0},
                {0, 0, 2, 1, 0},
                {0, 0, 1, 1, 0},
                {0, 0, 0, 0, 0}
            },
            {
                {0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0},
                {0, 0, 2, 1, 0},
                {0, 0, 1, 1, 0},
                {0, 0, 0, 0, 0}
            },
            {
                {0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0},
                {0, 0, 2, 1, 0},
                {0, 0, 1, 1, 0},
                {0, 0, 0, 0, 0}
            },
            {
                {0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0},
                {0, 0, 2, 1, 0},
                {0, 0, 1, 1, 0},
                {0, 0, 0, 0, 0}
            }
        },
        // I
        {
            {
                {0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0},
                {0, 1, 2, 1, 1},
                {0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0}
            },
            {
                {0, 0, 0, 0, 0},
                {0, 0, 1, 0, 0}, 
                {0, 0, 2, 0, 0},
                {0, 0, 1, 0, 0},
                {0, 0, 1, 0, 0}
            },
            {
                {0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0},
                {1, 1, 2, 1, 0},
                {0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0}
            },
            {
                {0, 0, 1, 0, 0},
                {0, 0, 1, 0, 0},
                {0, 0, 2, 0, 0},
                {0, 0, 1, 0, 0},
                {0, 0, 0, 0, 0}
            }
        },
        // L
        {
            {
                {0, 0, 0, 0, 0},
                {0, 0, 1, 0, 0},
                {0, 0, 2, 0, 0},
                {0, 0, 1, 1, 0},
                {0, 0, 0, 0, 0}
            },
            {
                {0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0},
                {0, 1, 2, 1, 0},
                {0, 1, 0, 0, 0},
                {0, 0, 0, 0, 0}
            },
            {
                {0, 0, 0, 0, 0},
                {0, 1, 1, 0, 0},
                {0, 0, 2, 0, 0},
                {0, 0, 1, 0, 0},
                {0, 0, 0, 0, 0}
            },
            {
                {0, 0, 0, 0, 0},
                {0, 0, 0, 1, 0},
                {0, 1, 2, 1, 0},
                {0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0}
            }
        },
        // L mirrored
        {
            {
                {0, 0, 0, 0, 0},
                {0, 0, 1, 0, 0},
                {0, 0, 2, 0, 0},
                {0, 1, 1, 0, 0},
                {0, 0, 0, 0, 0}
            },
            {
                {0, 0, 0, 0, 0},
                {0, 1, 0, 0, 0},
                {0, 1, 2, 1, 0},
                {0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0}
            },
            {
                {0, 0, 0, 0, 0},
                {0, 0, 1, 1, 0},
                {0, 0, 2, 0, 0},
                {0, 0, 1, 0, 0},
                {0, 0, 0, 0, 0}
            },
            {
                {0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0},
                {0, 1, 2, 1, 0},
                {0, 0, 0, 1, 0},
                {0, 0, 0, 0, 0}
            }
        },
        // N
        {
            {
                {0, 0, 0, 0, 0},
                {0, 0, 0, 1, 0},
                {0, 0, 2, 1, 0},
                {0, 0, 1, 0, 0},
                {0, 0, 0, 0, 0}
            },
            {
                {0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0},
                {0, 1, 2, 0, 0},
                {0, 0, 1, 1, 0},
                {0, 0, 0, 0, 0}
            },
            {
                {0, 0, 0, 0, 0},
                {0, 0, 1, 0, 0},
                {0, 1, 2, 0, 0},
                {0, 1, 0, 0, 0},
                {0, 0, 0, 0, 0}
            },
            {
                {0, 0, 0, 0, 0},
                {0, 1, 1, 0, 0},
                {0, 0, 2, 1, 0},
                {0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0}
            }
        },
        // N mirrored
        {
            {
                {0, 0, 0, 0, 0},
                {0, 0, 1, 0, 0},
                {0, 0, 2, 1, 0},
                {0, 0, 0, 1, 0},
                {0, 0, 0, 0, 0}
            },
            {
                {0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0},
                {0, 0, 2, 1, 0},
                {0, 1, 1, 0, 0},
                {0, 0, 0, 0, 0}
            },
            {
                {0, 0, 0, 0, 0},
                {0, 1, 0, 0, 0},
                {0, 1, 2, 0, 0},
                {0, 0, 1, 0, 0},
                {0, 0, 0, 0, 0}
            },
            {
                {0, 0, 0, 0, 0},
                {0, 0, 1, 1, 0},
                {0, 1, 2, 0, 0},
                {0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0}
            }
        },
        // T
        {
            {
                {0, 0, 0, 0, 0},
                {0, 0, 1, 0, 0},
                {0, 0, 2, 1, 0},
                {0, 0, 1, 0, 0},
                {0, 0, 0, 0, 0}
            },
            {
                {0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0},
                {0, 1, 2, 1, 0},
                {0, 0, 1, 0, 0},
                {0, 0, 0, 0, 0}
            },
            {
                {0, 0, 0, 0, 0},
                {0, 0, 1, 0, 0},
                {0, 1, 2, 0, 0},
                {0, 0, 1, 0, 0},
                {0, 0, 0, 0, 0}
            },
            {
                {0, 0, 0, 0, 0},
                {0, 0, 1, 0, 0},
                {0, 1, 2, 1, 0},
                {0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0}
            }
        }
    };

    // Displacement of the piece to the position where it is first drawn in the board when it is created
    const static uint8_t kPositionCount = 2;
    const static int16_t kPiecesInitialPosition[kKindCount][kRotationCount][kPositionCount] =
    {
        // Square
        {
            {-2, -3}, 
            {-2, -3},
            {-2, -3},
            {-2, -3}
        },
        // I
        {
            {-2, -2},
            {-2, -3},
            {-2, -2},
            {-2, -3}
        },
        // L
        {
            {-2, -3},
            {-2, -3},
            {-2, -3},
            {-2, -2}
        },
        // L mirrored
        {
            {-2, -3},
            {-2, -2},
            {-2, -3},
            {-2, -3}
        },
        // N
        {
            {-2, -3},
            {-2, -3},
            {-2, -3},
            {-2, -2}
        },
        // N mirrored
        {
            {-2, -3},
            {-2, -3},
            {-2, -3},
            {-2, -2}
        },
        // T
        {
            {-2, -3},
            {-2, -3},
            {-2, -3},
            {-2, -2}
        },
    };

    class Pieces
    {
    public:
        Pieces() {}
        ~Pieces() {}
        BlockType get_block_type(PieceType piece, uint8_t rotation, int16_t x, int16_t y)
        {
            return kPieces[piece][rotation][x][y];
        }
        int16_t get_x_initial_position(PieceType piece, uint8_t rotation)
        {
            return kPiecesInitialPosition[piece][rotation][0];
        }
        int16_t get_y_initial_position(PieceType piece, uint8_t rotation)
        {
            return kPiecesInitialPosition[piece][rotation][1];
        }
    };

    const static int16_t kBoardWidth = 10;  // Board width in blocks 
    const static int16_t kBoardHeight = 20; // Board height in blocks

    typedef uint8_t BoardBlock;
    const static BoardBlock kPosFree = 0;
    const static BoardBlock kPosFilled = 1;

    class Board
    {
    public:
        Board() {
            reset();
        }
        ~Board() {}
        void reset() {
            for (int16_t x = 0; x < kBoardWidth; x++) {
                for (int16_t y = 0; y < kBoardHeight; y++) {
                    board_[x][y] = kPosFree;
                }
            }
        }
        void store_piece(int16_t x, int16_t y, PieceType piece, uint8_t rotation)
        {
            // Store each block of the piece into the board
            for (int16_t i1 = x, i2 = 0; i1 < x + kHorizontalBlockCount; i1++, i2++)
            {
                for (int16_t j1 = y, j2 = 0; j1 < y + kVerticalBlockCount; j1++, j2++)
                {   
                    // Store only the blocks of the piece that are not holes
                    if (pieces_.get_block_type(piece, rotation, i2, j2) != kNoBlock) {
                        board_[i1][j1] = kPosFilled;
                    }
                }
            }
        }
        bool is_game_over()
        {
            // If the first line has blocks, then, game over
            for (int16_t i = 0; i < kBoardWidth; i++)
            {
                if (board_[i][0] == kPosFilled) {
                    return true;
                }
            }
            return false;
        }
        void delete_line(int16_t y)
        {
            // Moves all the upper lines one row down
            for (int16_t j = y; j > 0; j--)
            {
                for (int16_t i = 0; i < kBoardWidth; i++)
                {
                    board_[i][j] = board_[i][j-1];
                }
            }   
        }
        void delete_possible_lines()
        {
            for (int16_t j = 0; j < kBoardHeight; j++)
            {
                int16_t i = 0;
                while (i < kBoardWidth)
                {
                    if (board_[i][j] != kPosFilled) {
                        break;
                    }
                    i++;
                }
                if (i == kBoardWidth) {
                    delete_line(j);
                }
            }
        }
        bool is_free_block(int16_t x, int16_t y)
        {
            if (board_[x][y] == kPosFree) {
                return true;
            } else { 
                return false;
            }
        }
        bool is_possible_movement(int16_t x, int16_t y, PieceType piece, int16_t rotation)
        {
            // Checks collision with pieces already stored in the board or the board limits
            // This is just to check the 5x5 blocks of a piece with the appropriate area in the board
            for (int16_t i1 = x, i2 = 0; i1 < x + kHorizontalBlockCount; i1++, i2++)
            {
                for (int16_t j1 = y, j2 = 0; j1 < y + kVerticalBlockCount; j1++, j2++)
                {   
                    // Check if the piece is outside the limits of the board
                    if (    i1 < 0           || 
                        i1 > kBoardWidth  - 1    ||
                        j1 > kBoardHeight - 1)
                    {
                        if (pieces_.get_block_type(piece, rotation, i2, j2) != kNoBlock) {
                            return false;
                        }
                    }

                    // Check if the piece have collisioned with a block already stored in the map
                    if (j1 >= 0) 
                    {
                        if ((pieces_.get_block_type(piece, rotation, i2, j2) != 0) &&
                            (!is_free_block(i1, j1)) ) {
                            return false;
                        }
                    }
                }
            }
        
            // No collision
            return true;
        }
    private:
        Pieces pieces_;
        BoardBlock board_[kBoardWidth][kBoardHeight];
    };

    class Game
    {
    public:

        typedef std::function<void(void)> DrawClearFn;
        typedef std::function<void(int16_t,int16_t,PieceType)> DrawPieceFn;
        typedef std::function<void(int16_t,int16_t)> DrawBlockFn;
        typedef std::function<int16_t(int16_t,int16_t)> RandomFn;
        typedef std::function<void(void)> GameOverFn;

        Game() : draw_clear_(nullptr), draw_current_piece_(nullptr), draw_next_piece_(nullptr), draw_block_(nullptr), random_(nullptr), game_over_(nullptr) { reset(); }
        ~Game() {}
        void set_draw_clear(DrawClearFn func) {draw_clear_ = func;}
        void set_draw_current_piece(DrawPieceFn func) {draw_current_piece_ = func;}
        void set_draw_next_piece(DrawPieceFn func) {draw_next_piece_ = func;}
        void set_draw_block(DrawBlockFn func) {draw_block_ = func;}
        void set_random(RandomFn func) {random_ = func;}
        void set_game_over(GameOverFn func) {game_over_ = func;}
        int16_t random(int16_t min, int16_t max) {
            if (random_) {
                return random_(min, max);
            }
            return min;
        }
        void reset() {
            // First piece
            piece_          = static_cast<PieceType>(random(0, kKindCount - 1));
            rotation_       = random(0, kRotationCount - 1);
            pos_x_          = static_cast<int16_t>(kBoardWidth / 2) + pieces_.get_x_initial_position(piece_, rotation_);
            pos_y_          = pieces_.get_y_initial_position(piece_, rotation_);

            //  Next piece
            next_piece_     = static_cast<PieceType>(random(0, kKindCount - 1));
            next_rotation_  = random(0, kRotationCount - 1);
            pos_x_          = kBoardWidth + kHorizontalBlockCount;
            pos_y_          = kVerticalBlockCount;

            // Clear board
            board_.reset();
        }
        void create_new_piece() {
            // The new piece
            piece_          = next_piece_;
            rotation_       = next_rotation_;
            pos_x_          = static_cast<int16_t>(kBoardWidth / 2) + pieces_.get_x_initial_position(piece_, rotation_);
            pos_y_          = pieces_.get_y_initial_position(piece_, rotation_);

            //  Next piece
            next_piece_     = static_cast<PieceType>(random(0, kKindCount - 1));
            next_rotation_  = random(0, kRotationCount - 1);
        }
        void draw_clear() {
            if (draw_clear_) {
                draw_clear_();
            }
        }
        void draw_piece(int16_t x, int16_t y, PieceType piece, uint8_t rotation, DrawPieceFn &draw_piece_fn) {
            if (!draw_piece_fn) {
                return;
            }
            for (int16_t i = 0; i < kVerticalBlockCount; i++)
            {
                for (int16_t j = 0; j < kHorizontalBlockCount; j++)
                {
                    if (((x + j) < 0) || ((x + j) > kBoardWidth) || ((y + i) < 0) || ((y + i) > kBoardHeight)) {
                        // out of bounds
                        continue;
                    }
                    // Get the type of the block and draw it with the correct color
                    if (pieces_.get_block_type(piece, rotation, j, i) != kNoBlock) {
                        draw_piece_fn(x + j, y + i, piece);
                    }
                }
            }
        }
        void draw_board() {
            if (!draw_block_) {
                return;
            }
            for (int16_t x = 0; x < kBoardWidth; x++) {
                for (int16_t y = 0; y < kBoardHeight; y++) {
                    if (!(board_.is_free_block(x, y))) {
                        draw_block_(x, y);
                    }
                }
            }
        }
        void draw_scene() {
            draw_clear();
            draw_piece(0, 0, next_piece_, next_rotation_, draw_next_piece_);
            draw_board();
            draw_piece(pos_x_, pos_y_, piece_, rotation_, draw_current_piece_);
        }
        void key_left() {
            if (board_.is_game_over())
            {
                // reset required
                return;
            }
            if (board_.is_possible_movement(pos_x_ - 1, pos_y_, piece_, rotation_)) {
                pos_x_--;
                draw_scene();
            }
        }
        void key_right() {
            if (board_.is_game_over())
            {
                // reset required
                return;
            }
            if (board_.is_possible_movement(pos_x_ + 1, pos_y_, piece_, rotation_)) {
                pos_x_++;
                draw_scene();
            }
        }
        void key_down() {
            if (board_.is_game_over())
            {
                // reset required
                return;
            }
            if (board_.is_possible_movement(pos_x_, pos_y_ + 1, piece_, rotation_)) {
                pos_y_++;
                draw_scene();
            }
        }
        void key_drop() {
            if (board_.is_game_over())
            {
                // reset required
                return;
            }
            // Check collision from up to down
            while (board_.is_possible_movement(pos_x_, pos_y_ + 1, piece_, rotation_)) {
                pos_y_++;
            }
            board_.store_piece(pos_x_, pos_y_, piece_, rotation_);
            board_.delete_possible_lines();
            if (board_.is_game_over())
            {
                draw_scene();
                if (game_over_) {
                    game_over_();
                }
                return;
            }
            create_new_piece();
            draw_scene();
        }
        void key_rotate() {
            if (board_.is_game_over())
            {
                // reset required
                return;
            }
            if (board_.is_possible_movement(pos_x_, pos_y_, piece_, (rotation_ + 1) % kRotationCount)) {
                rotation_ = (rotation_ + 1) % kRotationCount;
                draw_scene();
            }
        }
        void tick() {
            if (board_.is_game_over())
            {
                // reset required
                return;
            }
            if (board_.is_possible_movement(pos_x_, pos_y_ + 1, piece_, rotation_))
            {
                pos_y_++;
                draw_scene();
            }
            else
            {
                board_.store_piece(pos_x_, pos_y_, piece_, rotation_);
                board_.delete_possible_lines();
                if (board_.is_game_over())
                {
                    draw_scene();
                    if (game_over_) {
                        game_over_();
                    }
                    return;
                }
                create_new_piece();
                draw_scene();
            }
        }
    private:
        DrawClearFn draw_clear_;
        DrawPieceFn draw_current_piece_;
        DrawPieceFn draw_next_piece_;
        DrawBlockFn draw_block_;
        RandomFn random_;
        GameOverFn game_over_;

        Pieces pieces_;
        Board board_;

        int16_t pos_x_, pos_y_;             // Position of the piece that is falling down
        PieceType piece_;                   // Kind of the piece that is falling down
        uint8_t rotation_;                  // Rotation of the piece that is falling down
        int16_t next_pos_x_, next_pos_y_;   // Position of the next piece
        PieceType next_piece_;              // Kind of the next piece
        uint8_t next_rotation_;             // Rotation of the next piece
    };
}

#endif
