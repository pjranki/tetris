Tetris
======
Platform independent Tetris.

Created by following the tutorial on http://javilop.com/gamedev/tetris-tutorial-in-c-platform-independent-focused-in-game-logic-for-beginners/

Language
--------
C++11 and up (should compile for anything, doesn't use any OS APIs).

Arduino
-------
Checkout/copy this library to your Arduino libraries folder.

How to use it
-------------
Include the library:

```cpp
#include <tetris.h>
```

Construct the class:

```cpp
tetris::Game tetris_game;
```

You can get the grid size for the game using the following constants:

```cpp
kBoardWidth;   // 10 blocks
kBoardHeight;  // 20 blocks
```

Tell the tetris game how to do things:

```cpp
tetris_game.set_draw_clear([]() {
    // Clear the screen of everything
});
tetris_game.set_draw_current_piece([](int16_t x, int16_t y, tetris::PieceType piece) {
    // Draw a single block of the piece at (x, y)
});
tetris_game.set_draw_next_piece([](int16_t x, int16_t y, tetris::PieceType piece) {
    // Draw a single block of the next piece at (x, y)
});
tetris_game.set_draw_block([](int16_t x, int16_t y) {
    // Draw a single block at (x, y)
    // This was a previous piece that is now stored on the board.
});
tetris_game.set_random([](int16_t min, int16_t max) -> int16_t {
    // return a random number between min and max (inclusive)
});
```

During the game, call the following functions when you get user input:

```cpp
tetris_game.key_left();
tetris_game.key_right();
tetris_game.key_down();
tetris_game.key_rotate();
tetris_game.key_drop();
```

Call the tick function every 700 milliseconds:

```cpp
tetris_game.tick();
```

To reset the game at any point, call the reset function:

```cpp
tetris_game.reset();
```

You can also handle the game over callback:

```cpp
tetris_game.set_game_over([](){
    // it might be a good idea just to reset the game
    tetris_game.reset();
});
```
